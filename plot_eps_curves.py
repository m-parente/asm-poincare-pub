import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)
sns.set_style('whitegrid')


data_curves_eps_theta_pi4 = np.loadtxt("data_curves_eps_theta_pi4.csv", delimiter=',')
data_curves_eps_qdr1 = np.loadtxt("data_curves_eps_qdr1.csv", delimiter=',')
data_curves_eps_theta_0 = np.ones(len(data_curves_eps_theta_pi4))[:, np.newaxis]

epss = data_curves_eps_theta_pi4[:, 0]

data_curves_eps_qdr4 = np.array([[(np.cos(theta)+np.sin(theta))**(-2) for eps in epss]
                                 for theta in [np.pi/8, np.pi/4]]).T

data = np.hstack([data_curves_eps_theta_pi4, data_curves_eps_qdr1[:, 2:4][:, ::-1],
                  data_curves_eps_theta_0, data_curves_eps_qdr4])

n_plot = np.shape(data)[1] - 1
xs = data[:, 0]
thetas = ["\pi/4", "3\pi/16", "\pi/8", "0", "-\pi/8", "-\pi/4"]
linestyles = ['-', '--', '-.', (0, (3, 2, 1, 2, 1, 2)), ':']

plt.figure()
ax = plt.gca()
plt.yscale('log')

for i in range(n_plot):
    plt.plot(xs, data[:, i+1], linestyle=linestyles[i % 5],
             linewidth=2, label='$\\theta=%s$' % thetas[i])

plt.xlabel("$\epsilon$")
plt.ylabel("$Q_\epsilon(\\theta)$")
plt.xticks(np.arange(0.0, 0.11, 0.02))
plt.legend()
plt.tight_layout()

plt.show()
