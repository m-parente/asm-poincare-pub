import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)
sns.set_style('whitegrid')


def sec(theta):
    return 1. / np.cos(theta)


def csc(theta):
    return 1. / np.sin(theta)


def varzy(theta, y):
    a = csc(theta)**4*sec(theta)**4
    b = -csc(theta) + sec(theta)
    c = np.cos(4*theta)
    d = np.sin(2*theta)

    return (a/(8*b**2)) * (((1-2*np.exp(b*y)+np.exp(2*b*y)-8*np.exp(b*y)*y**2*(1-d)) / (np.exp(b*y)-1)**2) - c)


ys = np.logspace(0, 5, 1000)
plt.figure()
plt.loglog(ys, varzy(1.9*np.pi/8, ys), '-', linewidth=2, label="$\\theta=1.9\pi/8$")
plt.loglog(ys, varzy(1.99*np.pi/8, ys), '--', linewidth=2, label="$\\theta=1.99\pi/8$")
plt.loglog(ys, varzy(1.999*np.pi/8, ys), '-.', linewidth=2, label="$\\theta=1.999\pi/8$")
plt.loglog(ys, ys**2/3, linestyle=(0, (3, 2, 1, 2, 1, 2)),
           linewidth=2, label="$y^2/3$ ($\\theta=\pi/4$)")
plt.xlabel('$y$')
plt.ylabel('$Var(Z|Y=y)$')
plt.legend()
plt.tight_layout()

plt.show()
