import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)
sns.set_style('whitegrid')


def c_tot(eps, n, k):
    return (n-k)**(1/(1+eps)) * ((np.math.gamma(n+2+2/eps)/np.math.factorial(n-1)) *
                                 sum([((k+i)/(k+i-1))**((1+eps)/eps) for i in range(1, n-k+1)]))**(eps/(1+eps))


def c_tot_eps(n, k):
    return lambda eps: c_tot(eps, n, k)


ns = [2, 5, 10, 20]
epss = np.linspace(0.014, 1, 1000)
linestyles = ['-', '--', '-.', ':']


plt.figure(figsize=(14, 6))
plt.subplot(1, 2, 1)
plt.yscale('log')
for i in range(len(ns)):
    n = ns[i]
    plt.plot(epss, [c_tot_eps(n, 1)(eps) for eps in epss],
             linestyle=linestyles[i], linewidth=2, label='$n=%i$' % n)
plt.xlabel('$\epsilon$')
plt.ylabel('$C_\epsilon(n,k=1)$')
plt.legend()
plt.tight_layout()


def c_tot_n(eps, k):
    return lambda n: c_tot(eps, n, k)


ns = [2] + list(range(5, 51, 5))
epss = [0.2, 0.1, 0.05, 0.02]

plt.subplot(1, 2, 2)
plt.yscale('log')
for i in range(len(epss)):
    eps = epss[i]
    plt.plot(ns, [c_tot_n(eps, 1)(n) for n in ns], 'o-',
             linestyle=linestyles[i], linewidth=2, label='$\epsilon=%.2f$' % eps)
plt.xlabel('$n$')
plt.ylabel('$C_\epsilon(n,k=1)$')
plt.xticks(ns)
plt.legend()
plt.tight_layout()

plt.subplots_adjust(wspace=0.25)

plt.show()
