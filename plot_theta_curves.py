import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

sns.set()
sns.set_context('paper', font_scale=1.5)
sns.set_style('whitegrid')


epss = [2.0, 1.0, 0.5, 0.1, 0.05]

data_curves_theta_qdr1_1 = np.loadtxt("data_curves_theta_qdr1_1.csv", delimiter=',')
data_curves_theta_qdr1_2 = np.loadtxt("data_curves_theta_qdr1_2.csv", delimiter=',')
data_curves_theta_qdr1 = np.concatenate([data_curves_theta_qdr1_1,
                                         data_curves_theta_qdr1_2], axis=0)

data_curves_theta_qdr4 = np.array([np.concatenate([[theta], [(np.cos(-theta)+np.sin(-theta))**(-2) for eps in epss]])
                                   for theta in np.linspace(-np.pi/2, 0, 100)])

data = np.concatenate([data_curves_theta_qdr4, data_curves_theta_qdr1], axis=0)

n_plot = np.shape(data)[1] - 1
xs = data[:, 0]
linestyles = ['-', '--', '-.', (0, (3, 2, 1, 2, 1, 2)), ':']
epss = [str(eps) for eps in epss]

plt.figure()
ax = plt.gca()
plt.yscale('log')

for i in range(n_plot):
    plt.plot(xs, data[:, i+1], linestyle=linestyles[i],
             linewidth=2, label="$\epsilon=%s$" % epss[i])

plt.xlabel("$\\theta$")
plt.ylabel("$Q_\epsilon(\\theta)$")
plt.xticks([-np.pi/2, -np.pi/4, 0, np.pi/4, np.pi/2])
ax.set_xticklabels(["$-\pi/2$", "$-\pi/4$", "0", "$\pi/4$", "$\pi/2$"])
plt.legend()
plt.tight_layout()

plt.show()
